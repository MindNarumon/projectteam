/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
/**
 *
 * @author acer
 */
public class Project {
    static Scanner kb = new Scanner(System.in);
	
	static String rec_username;
	static String rec_password;
	static boolean foundID = false, foundPass = false;
        static ArrayList<Person> arr = new ArrayList<>();
        
        static String a1, a2, a3, a4, e1, e2, e3, e4;
	static double a5, a6, e5, e6;


        
        public static void main(String[] args) {
		load();
		sayLogin();
	}
public static void load() {
		arr.add(new Person("admin", "1234", "kevin", "kyle", 178, 75));
		arr.add(new Person("mind", "4567", "narumon", "yujaroen", 158, 38));
	}


        
        public static void sayLogin() {
		System.out.println("Please input your username and password");
		System.out.print("Username : ");
		rec_username = kb.next();
		System.out.print("Password : ");
		rec_password = kb.next();
		checkLogin();
	}

	public static void checkLogin() {
		for (Person a : arr) {
			if (rec_username.equals(a.getUsername())) {
				foundID = true;
				if (rec_password.equals(a.getPassword())) {
					foundPass = true;
					break;
				} else {
					foundPass = false;
					break;
				}
			} else {
				foundID = false;
			}
		}
		checkLoginFound();
	}

	public static void checkLoginFound() {
		if (foundID == true && foundPass == true) {
			showSuccess();
			sayMenu();
		} else if (foundID == true && foundPass == false) {
			showErrorLoginPassword();
			sayLogin();
		} else {
			showErrorLoginUsername();
			sayLogin();
		}
	}

    private static void showSuccess() {
        System.out.println("Success.");
    }

    private static void showErrorLoginPassword() {
        System.err.println("Password is incorrect.");
    }

    private static void showErrorLoginUsername() {
        System.err.println("User is incorrect.");
    }
 private static void sayMenu() {
    System.out.println("Menu Panel");
		System.out.println("1. Add");
		System.out.println("2. Show");
		System.out.println("3. Edit");
		System.out.println("4. Log Out");
		System.out.println("5. Exit");
		System.out.println("");
		checkMenu();
    }
 public static void checkMenu() {
		showPleaseChoose();
		String input = kb.next();
		switch (input) {
		case "1":
			sayAdd();
			break;
		case "2":
			sayShow();
			break;
		case "3":
			sayEditCheckPass();
			break;
		case "4":
			logOut();
			break;
		case "5":
			System.exit(0);
			break;
		default:
			showErrorInput();
			sayMenu();
		}
	}

    private static void showPleaseChoose() {
        System.out.print("Please choose : ");
    }

    private static void sayAdd() {
        System.out.println("Please input information");
		System.out.println("1. Username 2. Password 3. Firstname 4. Lastname 5. Weight 6. Height");
		System.out.println("(0) back");
		add();

    }
private static void add() {
        chooseAddUser();
		chooseAddPass();
		chooseAddFname();
		chooseAddLname();
		chooseAddWeight();
		chooseAddHeight();
		addSave();
    }
public static void chooseAddUser() {
		System.out.print("username : ");
		a1 = kb.next();
		if (a1.equals("0")) {
			sayMenu();
		} else {
			for (Person a : arr) {
				if (a.getUsername().equals(a1)) {
					showErrorAddUser();
					chooseAddUser();
				}
			}
			System.out.println("Username can use.");
		}

	}

	public static void chooseAddWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight (cannot be zero) : ");
				a5 = kb.nextDouble();
				if (a5 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorWeight();
				kb.next();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseAddHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height (cannot be zero) : ");
				a6 = kb.nextDouble();
				if (a6 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorHeight();
				kb.next();
			}
		}
		System.out.println("Height can use.");
	}

	public static void chooseAddPass() {
		System.out.print("password : ");
		a2 = kb.next();
		if (a2.equals("0"))
			sayMenu();
	}

	public static void chooseAddFname() {
		System.out.print("firstname : ");
		a3 = kb.next();
		if (a3.equals("0"))
			sayMenu();
	}

	public static void chooseAddLname() {
		System.out.print("lastname : ");
		a4 = kb.next();
		if (a4.equals("0"))
			sayMenu();
	}
        private static void addSave() {
               System.out.println("(0) back / (Y) save");
		showPleaseChoose();
		String input = kb.next();
		if (input.equalsIgnoreCase("Y")) {
			arr.add(new Person(a1, a2, a3, a4, a5, a6));
			showSuccess();
			sayMenu();
		} else if (input.equals("0")) {
			sayAdd();
		} else
			addSave();

        }
        public static void addSetDefault() {
		a1 = "";
		a2 = "";
		a3 = "";
		a4 = "";
		a5 = 0;
		a6 = 0;
	}


    private static void showErrorInput() {
        System.err.println("Input didn't match.");
    }
    private static void showErrorHeight() {
        System.err.println("Height  must be number.");
    }

    private static void showErrorAddUser() {
        System.err.println("If Username has been exist. This message will show.");
    }

    private static void showErrorWeight() {
        System.err.println("Weight must be number.");
    }

    private static void sayShow() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void sayEditCheckPass() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void logOut() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    


   

    

}
class Person {
    
    String Username;
    String Password;
    String Firstname;
    String Lastname;
    double Weight;
    double Height;
    
Person(String Username,String Password,String Firstname,String Lastname,double Weight,double Height) {
    this.Username = Username;
    this.Password = Password;
    this.Firstname = Firstname;
    this.Lastname = Lastname;
    this.Weight = Weight;
    this.Height = Height;
    }   

public String getUsername() {
    return Username;
}
public String getPassword() {
    return Password;
}
public String getFirst() {
    return Firstname;
}
public String getLast() {
    return Lastname;
}
public Double getWeight() {
    return Weight;
}public Double getHeight() {
    return Height;
}
}